package kh.edu.rupp.ite.accessories_shop_project

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*

import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.squareup.picasso.Picasso
import kh.edu.rupp.ite.accessories_shop_project.Home.*
import kh.edu.rupp.ite.accessories_shop_project.databinding.ActivityMainBinding
import kh.edu.rupp.ite.accessories_shop_project.model.Product
import kotlinx.android.synthetic.main.activity_product_detail.*
import kotlinx.android.synthetic.main.activity_product_detail.view.*
import kotlinx.android.synthetic.main.activity_sign_in.*


class ProductDetailActivity: AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private lateinit var colorSpinner: Spinner
    var color =
        arrayOf<String>("Red", "White", "Green", "Gray", "Black", "Blue", "Pink", "Yellow")
    private var currentQuantity = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)

        val selectedProduct = intent.getParcelableExtra<Product>("PRODUCT_KEY")


        colorSpinner = findViewById(R.id.spinnerColor)

        colorSpinner.onItemSelectedListener = this
        val adapter:ArrayAdapter<CharSequence> = ArrayAdapter(this,android.R.layout.simple_spinner_item,color)

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        colorSpinner.adapter = adapter

        val selection = "Red"

        val spinnerPosition: Int = adapter.getPosition(selection)

        Log.e("spinner","$spinnerPosition")

        colorSpinner.setSelection(spinnerPosition)



        populateProductDetails(selectedProduct)







    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
//        Toast.makeText(this," " + color.get(position) + "Selected ..", Toast.LENGTH_SHORT).show()
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("Not yet implemented")
    }



    private fun populateProductDetails(product: Product?) {
        // Populate the UI with product details


        if (product != null) {
            productName1.text = product.name.uppercase()
            productDes1.text = product.description
            productPrice1.text = "$${product.price}"
//            Picasso.get().load(product.img).into(ImageProduct)
                    Picasso.get().load(product.img).into(productImage1)
            // Add more details as needed



        }

        buttonAddToCard.setOnClickListener {
            val intent = Intent(this,ProductAddToBuy::class.java).apply {
                putExtra("PRODUCT_KEY", product)
            }
            startActivity(intent)
        }
        buttonBack.setOnClickListener { finish() }
    }


}
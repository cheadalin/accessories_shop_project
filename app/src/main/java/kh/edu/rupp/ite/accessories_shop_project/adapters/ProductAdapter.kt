package kh.edu.rupp.ite.accessories_shop_project.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kh.edu.rupp.ite.accessories_shop_project.Home
import kh.edu.rupp.ite.accessories_shop_project.R
import kh.edu.rupp.ite.accessories_shop_project.model.Product
import kotlinx.android.synthetic.main.fragment_profile.view.*

class ProductAdapter(private var productList: List<Product>) :

    RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {


    var onItemClickListener: ((Product) -> Unit)? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {

//        val view =
//            LayoutInflater.from(parent.context).inflate(R.layout., parent, false)

//        val view = inflater.inflate(R.layout.fragment_home, container, false)
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_product, parent, false)
        return ProductViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val product = productList[position]
        holder.bind(product)



        holder.itemView.setOnClickListener{
            onItemClickListener?.invoke(product)
        }
    }

    override fun getItemCount(): Int {
        return productList.size
    }

    fun updateData(newList: List<Product>) {
        productList = newList
        notifyDataSetChanged()
    }



    class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val imageProduct : ImageView = itemView.findViewById(R.id.productImage)
        private val nameTextView: TextView = itemView.findViewById(R.id.productName)
//        private val descriptionTextView: TextView = itemView.findViewById(R.id.)
        private val priceTextView: TextView = itemView.findViewById(R.id.productPrice)

        fun bind(product: Product) {
            Picasso.get().load(product.img).into(imageProduct)
            nameTextView.text = product.name
//            descriptionTextView.text = product.description
            priceTextView.text = "$${product.price}"
        }
    }
}
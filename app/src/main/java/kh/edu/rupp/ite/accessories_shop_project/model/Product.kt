package kh.edu.rupp.ite.accessories_shop_project.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Product(
    var id: String? = null,
    val img: String?,
    val name: String, val description: String?, val price: Double
) : Parcelable {
    // Add a default (no-argument) constructor
    constructor() : this("", "",  "","", 0.0)
}
package kh.edu.rupp.ite.accessories_shop_project

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import kh.edu.rupp.ite.accessories_shop_project.databinding.FragmentSettingsBinding
import kotlinx.android.synthetic.main.fragment_settings.*
import kotlin.jvm.internal.Ref

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Settings.newInstance] factory method to
 * create an instance of this fragment.
 */
class Settings : Fragment() {


    private var _binding : FragmentSettingsBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentSettingsBinding.inflate(inflater,container,false)

        val darkModeSwitch : Switch = binding.darkModeSwitch

        darkModeSwitch.isChecked = isDarkMode()

        darkModeSwitch.setOnCheckedChangeListener{_,isChecked ->

            if (isChecked){
                setMode(AppCompatDelegate.MODE_NIGHT_NO)
            }
            else {
                // Enable Light Mode
                setMode(AppCompatDelegate.MODE_NIGHT_YES)
            }
        }

            return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun isDarkMode(): Boolean {
        return AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES
    }

    private fun setMode(mode: Int) {
        AppCompatDelegate.setDefaultNightMode(mode)
    }

    companion object {

    }



}

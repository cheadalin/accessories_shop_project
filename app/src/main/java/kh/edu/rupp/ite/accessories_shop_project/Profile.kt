package kh.edu.rupp.ite.accessories_shop_project

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kh.edu.rupp.ite.accessories_shop_project.databinding.FragmentProfileBinding
import kh.edu.rupp.ite.accessories_shop_project.databinding.FragmentSettingsBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Profile.newInstance] factory method to
 * create an instance of this fragment.
 */
class Profile : Fragment() {
    private lateinit var user : FirebaseAuth
    private var _binding : FragmentProfileBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        _binding = FragmentProfileBinding.inflate(inflater,container,false)

        user = FirebaseAuth.getInstance()
        binding.apply {
            buttonLogout.setOnClickListener {
                signOut()
            }

        }
        if(user.currentUser !== null){
            user.currentUser?.let {
                binding.tvEmail.text = it.email
            }
        }

        return binding.root
    }

    private fun signOut() {
        FirebaseAuth.getInstance().signOut()
        val signInIntent = Intent(requireContext(), SignInActivity::class.java)
        Toast.makeText(context,"Log Out Successfully", Toast.LENGTH_SHORT).show()
        startActivity(signInIntent)
        requireActivity().finish()
    }
}
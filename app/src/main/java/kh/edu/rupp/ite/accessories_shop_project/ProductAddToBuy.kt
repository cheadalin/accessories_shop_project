package kh.edu.rupp.ite.accessories_shop_project

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import kh.edu.rupp.ite.accessories_shop_project.model.Product
import kotlinx.android.synthetic.main.activity_product_add_to_buy.*
import kotlinx.android.synthetic.main.activity_product_detail.*

class ProductAddToBuy: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_add_to_buy)
        val selectedProduct = intent.getParcelableExtra<Product>("PRODUCT_KEY")

        populateProductDetails(selectedProduct)
    }


    private fun populateProductDetails(product: Product?) {
        // Populate the UI with product details
        Log.e("product","$product")


        if (product != null) {
            titleImage.text = product.name.uppercase()
            imagePrice.text = "$${product.price}"

            Picasso.get().load(product.img).into(productImage2)



        }
        buttonBack2.setOnClickListener {
            val intent = Intent(this,ProductDetailActivity::class.java).apply {
                putExtra("PRODUCT_KEY", product)
            }
            startActivity(intent)
        }
        buttonConfirm.setOnClickListener {
            val intent = Intent(this,MainActivity::class.java).apply {
                putExtra("PRODUCT_KEY", product)
            }
            startActivity(intent)
        }

    }
}
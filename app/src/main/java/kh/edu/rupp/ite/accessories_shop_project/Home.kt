package kh.edu.rupp.ite.accessories_shop_project

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.denzcoskun.imageslider.constants.ScaleTypes
import com.denzcoskun.imageslider.models.SlideModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kh.edu.rupp.ite.accessories_shop_project.adapters.ProductAdapter
import kh.edu.rupp.ite.accessories_shop_project.databinding.FragmentHomeBinding
import kh.edu.rupp.ite.accessories_shop_project.model.DataProduct
import kh.edu.rupp.ite.accessories_shop_project.model.Product
import kotlinx.android.parcel.Parcelize


class Home : Fragment() {


    private lateinit var recyclerView: RecyclerView
    private lateinit var productAdapter: ProductAdapter
    private lateinit var databaseReference: DatabaseReference
    private lateinit var productList: MutableList<Product>
    private lateinit var user : FirebaseAuth
    private var _binding : FragmentHomeBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater,container,false)


        val view = binding.root
        user = FirebaseAuth.getInstance()

        var imageList = ArrayList<SlideModel>()


        imageList.add(SlideModel("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRRgRax9kX0iqfpCao4vmgerTeMqG8NemL8ZGt328Cv5nztfay1PLCI-ry5-8uN7YOZmQ&usqp=CAU"))
        imageList.add(SlideModel("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVRAvPqJnH2nebN1hvFKEUMWIJGOIk11-egaDCSsaC7QbkJ6IuP-7otSSvwSD3AN6frg&usqp=CAU",))
        imageList.add(SlideModel("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSNB92dEUFUuK3bOT-aAGfAkdN5zY_KBv_RkWOvdwxaFqnJuSz0yaf1VtRRoE16tRUsDA&usqp=CAU"))

        binding.imageSlider?.setImageList(imageList,ScaleTypes.FIT)

        recyclerView = binding.recyclerView
        recyclerView.layoutManager = GridLayoutManager(context, 2)
        recyclerView.setHasFixedSize(true)

        getUserData()
        return binding.root




    }

    private fun getUserData() {
        databaseReference = FirebaseDatabase.getInstance().getReference("products")
        databaseReference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    val productList = mutableListOf<Product>()
                    for (userSnapshot in snapshot.children) {
                        val user = userSnapshot.getValue(Product::class.java)
//                        Log.e("FirebaseData", "Product: $user")
                        productList.add(user!!)
                        productAdapter = ProductAdapter(productList)
                        productAdapter.onItemClickListener = { selectedProduct ->
                            // Handle the click event, e.g., navigate to the product detail screen
                            showProductDetail(selectedProduct)
                        }
                        recyclerView.adapter = productAdapter
                    }

                }
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e("FirebaseError", "Error getting data", error.toException())
                Toast.makeText(context, "Error getting data", Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    private fun showProductDetail(product: Product) {
        val intent = Intent(requireContext(), ProductDetailActivity::class.java).apply {
            putExtra("PRODUCT_KEY", product)
        }
        startActivity(intent)
    }


    companion object {

    }


}
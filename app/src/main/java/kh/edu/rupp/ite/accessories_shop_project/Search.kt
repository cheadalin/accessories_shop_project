package kh.edu.rupp.ite.accessories_shop_project
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.SearchView
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kh.edu.rupp.ite.accessories_shop_project.adapters.ProductAdapter
import kh.edu.rupp.ite.accessories_shop_project.databinding.FragmentHomeBinding
import kh.edu.rupp.ite.accessories_shop_project.databinding.FragmentSearchBinding
import kh.edu.rupp.ite.accessories_shop_project.model.DataProduct
import kh.edu.rupp.ite.accessories_shop_project.model.Product
import kotlinx.android.synthetic.main.activity_sign_in.view.*


class Search : Fragment() {

    private lateinit var searchEditText: SearchView
    private lateinit var searchButton : Button
    private lateinit var recyclerView: RecyclerView
    private lateinit var productAdapter: ProductAdapter
    private lateinit var productList: List<Product>
    private lateinit var user : FirebaseAuth
    private lateinit var databaseReference: DatabaseReference
    private var _binding: FragmentSearchBinding? = null
    private val binding get() = _binding!!


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        _binding = FragmentSearchBinding.inflate(inflater,container,false)
        databaseReference = FirebaseDatabase.getInstance().getReference("products")

        searchEditText = binding.searchEditText
        searchButton = binding.searchButton
        recyclerView = binding.searchResultsRecyclerView



//        recyclerView.setHasFixedSize(true)





        searchButton.setOnClickListener {
            performSearch()
        }



        return binding.root
    }


    private fun performSearch() {
        Log.e("test","test")

        val productQuery =  searchEditText.query.toString()



        val query : Query = databaseReference.orderByChild("name").startAt(productQuery).endAt(productQuery + "\uf8ff")
//
        Log.e("query", "query: $query")
        Log.e("databaseReference", "databaseReference: $databaseReference")
        Log.e("productQuery", "productQuery: $productQuery")
        query.addValueEventListener(object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                Log.e("hd","hd")
                val productList = mutableListOf<Product>()

                Log.e("search","$productList")


                for (productSnapshot in dataSnapshot.children) {
                    // Deserialize the product from the snapshot
                    val product1 = productSnapshot.getValue(Product::class.java)

                    Log.e("product","$product1")

                    // Check if the product matches the search criteria
                    if (product1 != null && product1.name.contains(productQuery, ignoreCase = true)) {
                        // Add the matching product to the list
                        productList.add(product1)
                    }

                    Log.e("productList","ProductList : $productList")


//                    val adapter = ProductAdapter(searchResults)

                    productAdapter = ProductAdapter(productList)

                    recyclerView.adapter = productAdapter
                    recyclerView.layoutManager = GridLayoutManager(context, 2)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Log.e("FirebaseSearch", "Error: ${databaseError.message}")
                // Handle errors
                // ...
            }
        })



    }


    companion object {

    }
}
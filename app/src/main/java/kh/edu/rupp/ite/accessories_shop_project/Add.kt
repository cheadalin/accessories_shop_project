package kh.edu.rupp.ite.accessories_shop_project


import android.app.Activity
import android.content.ContentValues.TAG
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kh.edu.rupp.ite.accessories_shop_project.databinding.FragmentAddBinding
import kh.edu.rupp.ite.accessories_shop_project.model.Product
import java.util.*
import kotlin.math.log

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Home.newInstance] factory method to
 * create an instance of this fragment.
 */
class Add : Fragment() {
    // TODO: Rename and change types of parameters

    private var  _binding : FragmentAddBinding? = null
    private val binding get() = _binding!!

    private lateinit var etProductName : EditText
    private lateinit var etProductDes : EditText
    private lateinit var etProductPrice: EditText
    private lateinit var btnAddButton: Button
    private lateinit var btnSelectImg : Button
    private lateinit var imagePreview : ImageView

    private var selectedImageUri: Uri? = null


    private lateinit var firebaseRef: DatabaseReference
    private lateinit var storageRef: StorageReference


    private lateinit var imagePicker: ActivityResultLauncher<Intent>

    companion object {
        private const val REQUEST_IMAGE_PICK = 123
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAddBinding.inflate(inflater,container,false)
//        firebaseRef = FirebaseDatabase.getInstance().getReference("contacts")
//        storageRef = FirebaseStorage.getInstance().getReference("product_images")

        etProductName = binding.edtName
        etProductDes = binding.edtDescription
        etProductPrice = binding.edtPrice

        btnSelectImg = binding.btnPickImage
        btnAddButton = binding.btnAdd
        imagePreview = binding.imgAdd

        storageRef = FirebaseStorage.getInstance().reference.child("product_images")
        firebaseRef = FirebaseDatabase.getInstance().reference.child("products")

        imagePicker = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){
            result ->
            if(result.resultCode == Activity.RESULT_OK){
                result.data?.data?.let {
                    selectedImageUri = it
                    imagePreview.setImageURI(selectedImageUri)
                }
            }
        }

        btnSelectImg.setOnClickListener { selectImage() }
        btnAddButton.setOnClickListener { addItem() }
//        binding.btnSend.setOnClickListener {
//            saveData()
//        }




        return binding.root
    }



    private fun selectImage() {
        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        imagePicker.launch(galleryIntent)
    }



    private fun addItem() {
        val productName = etProductName.text.toString()
        val productDescription = etProductDes.text.toString()
        val productPrice = etProductPrice.text.toString().toDoubleOrNull()


        if (!productName.isNullOrEmpty() && productPrice != null) {
            if (selectedImageUri != null) {
                uploadImage(selectedImageUri!!, productName, productDescription, productPrice)
            } else {
                saveProductToFirebase(null, productName, productDescription, productPrice)
            }
        } else {
            // Handle invalid input (e.g., show an error message)
        }
    }

    private fun uploadImage(imageUri: Uri, productName: String, productDescription: String, productPrice: Double) {
        val imageRef = storageRef.child("${UUID.randomUUID()}.jpg")
        val uploadTask: UploadTask = imageRef.putFile(imageUri)

        uploadTask.addOnSuccessListener {
            imageRef.downloadUrl.addOnSuccessListener { uri ->
                saveProductToFirebase(uri.toString(), productName, productDescription, productPrice)
            }
        }.addOnFailureListener {
            // Handle unsuccessful uploads
            Toast.makeText(context,"error upload",Toast.LENGTH_SHORT).show()
        }
    }

    private fun saveProductToFirebase(imageUrl: String?, productName: String, productDescription: String, productPrice: Double) {
        Log.e("name",productName)
        Log.e("des",productDescription)
        Log.e("price", productPrice.toString())
        val newProduct = Product(null,imageUrl, productName, productDescription, productPrice, )
        val productKey = firebaseRef.push().key

        productKey?.let {
            newProduct.id = it
            firebaseRef.child(it).setValue(newProduct)
        }

        // Clear input fields after adding the item
        etProductName.text.clear()
        etProductDes.text.clear()
        etProductPrice.text.clear()
//        imagePreview.setImageResource(android.R.drawable.ic_menu_gallery)
        imagePreview.setImageResource(android.R.drawable.alert_light_frame)
    }



}
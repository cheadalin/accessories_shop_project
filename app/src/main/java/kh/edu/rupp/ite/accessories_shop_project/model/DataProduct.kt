package kh.edu.rupp.ite.accessories_shop_project.model

import kh.edu.rupp.ite.accessories_shop_project.Home

object DataProduct{
    private val productList: MutableList<Product> = mutableListOf()

    init {
        productList.add(Product("1","https://media.istockphoto.com/id/171224469/photo/canvas-shoes.webp?b=1&s=612x612&w=0&k=20&c=U9NLTutbuQ_L3HABoaa9Bsb_U3iJYw1QmKCBe0H3KWM=","Yellow Shoes", "A pair of green/purple iridescent high top shoes with black rubber soles and black laces. The wearer rests her feet on a wheelchair footrest", 19.99))
        productList.add(Product("1","https://media.istockphoto.com/id/1350560575/photo/pair-of-blue-running-sneakers-on-white-background-isolated.webp?b=1&s=170667a&w=0&k=20&c=liUSgX6SafJ7HWvefFqR9-pnf3KuH6v1lwQ6iEpePWc=","Sky Shoes", "A pair of green/purple iridescent high top shoes with black rubber soles and black laces. The wearer rests her feet on a wheelchair footrest", 29.99))
        productList.add(Product("1","https://contents.mediadecathlon.com/p2153181/be8bc3d9588b54af503e67bfa50b498b/p2153181.jpg?format=auto&quality=70&f=650x0","Blue Shoes", "A pair of green/purple iridescent high top shoes with black rubber soles and black laces. The wearer rests her feet on a wheelchair footrest", 39.99))
        productList.add(Product("1","https://media.istockphoto.com/id/171224469/photo/canvas-shoes.webp?b=1&s=612x612&w=0&k=20&c=U9NLTutbuQ_L3HABoaa9Bsb_U3iJYw1QmKCBe0H3KWM=","Yellow Shoes", "A pair of green/purple iridescent high top shoes with black rubber soles and black laces. The wearer rests her feet on a wheelchair footrest", 19.99))
        productList.add(Product("1","https://media.istockphoto.com/id/171224469/photo/canvas-shoes.webp?b=1&s=612x612&w=0&k=20&c=U9NLTutbuQ_L3HABoaa9Bsb_U3iJYw1QmKCBe0H3KWM=","Yellow Shoes", "A pair of green/purple iridescent high top shoes with black rubber soles and black laces. The wearer rests her feet on a wheelchair footrest", 19.99))
        productList.add(Product("1","https://media.istockphoto.com/id/171224469/photo/canvas-shoes.webp?b=1&s=612x612&w=0&k=20&c=U9NLTutbuQ_L3HABoaa9Bsb_U3iJYw1QmKCBe0H3KWM=","Yellow Shoes", "A pair of green/purple iridescent high top shoes with black rubber soles and black laces. The wearer rests her feet on a wheelchair footrest", 19.99))
        productList.add(Product("1","https://media.istockphoto.com/id/1350560575/photo/pair-of-blue-running-sneakers-on-white-background-isolated.webp?b=1&s=170667a&w=0&k=20&c=liUSgX6SafJ7HWvefFqR9-pnf3KuH6v1lwQ6iEpePWc=","Sky Shoes", "A pair of green/purple iridescent high top shoes with black rubber soles and black laces. The wearer rests her feet on a wheelchair footrest", 29.99))
        productList.add(Product("1","https://contents.mediadecathlon.com/p2153181/be8bc3d9588b54af503e67bfa50b498b/p2153181.jpg?format=auto&quality=70&f=650x0","Blue Shoes", "A pair of green/purple iridescent high top shoes with black rubber soles and black laces. The wearer rests her feet on a wheelchair footrest", 39.99))
        productList.add(Product("1","https://media.istockphoto.com/id/171224469/photo/canvas-shoes.webp?b=1&s=612x612&w=0&k=20&c=U9NLTutbuQ_L3HABoaa9Bsb_U3iJYw1QmKCBe0H3KWM=","Yellow Shoes", "A pair of green/purple iridescent high top shoes with black rubber soles and black laces. The wearer rests her feet on a wheelchair footrest", 19.99))
        productList.add(Product("1","https://media.istockphoto.com/id/1350560575/photo/pair-of-blue-running-sneakers-on-white-background-isolated.webp?b=1&s=170667a&w=0&k=20&c=liUSgX6SafJ7HWvefFqR9-pnf3KuH6v1lwQ6iEpePWc=","Sky Shoes", "A pair of green/purple iridescent high top shoes with black rubber soles and black laces. The wearer rests her feet on a wheelchair footrest", 29.99))
        productList.add(Product("1","https://contents.mediadecathlon.com/p2153181/be8bc3d9588b54af503e67bfa50b498b/p2153181.jpg?format=auto&quality=70&f=650x0","Blue Shoes", "A pair of green/purple iridescent high top shoes with black rubber soles and black laces. The wearer rests her feet on a wheelchair footrest", 39.99))
    }

    fun getAllProducts(): List<Product> {
        return productList.toList()
    }
//
//    fun getProductById(productId: String): Home.Product? {
//        return productList.find { it.id == productId }
//    }

    fun addProduct(product: Product) {
        productList.add(product)
    }
//
//    fun updateProduct(product: Home.Product) {
//        val index = productList.indexOfFirst { it.id == product.id }
//        if (index != -1) {
//            productList[index] = product
//        }
//    }
//
//    fun deleteProduct(productId: String) {
//        productList.removeAll { it.id == productId }
//    }
}